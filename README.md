# TeamDesigner

#### 介绍
国产免费的轻量级团队协作软件，主要包含项目计划管理、数据库表在线设计、缺陷管理、在线EXCEL等，支持团队协作.

体验地址：http://team.nyawei.cn/

#### 软件架构
软件架构说明
前端
VUE
element ui

后台
spring boot
springmvc
mybatis
redis


#### 主要功能

1.  项目计划管理
1）项目计划跟进
2）项目日历
3）人员任务看板
4）状态看板

![输入图片说明](image.png)
![输入图片说明](image1.png)

2.  数据库设计
一、支持mysql,oracle等主流数据库
二、支持表导入，导出
三、支持模块化管理，模板一键生成
四、支持代码生成，ER关系图
五、其他功能等
![输入图片说明](image2.png)
![输入图片说明](image3.png)

3.  缺陷管理
![输入图片说明](image4.png)
![输入图片说明](image5.png)





